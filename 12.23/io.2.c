#include <stdio.h>
#include <stdlib.h>

enum Format
{
    CHINESE,
    MONTH_FIRST,
    YEAR_FIRST,
    WRONG
};

struct Date
{
    int year;
    int month;
    int day;
    enum Format format;
};

void printDate(struct Date date)
{
    switch (date.format)
    {
    case CHINESE:
        printf("%d年%d月%d日\n", date.year, date.month, date.day);
        break;
    case MONTH_FIRST:
        printf("%d/%d/%d\n", date.month, date.day, date.year);
        break;
    case YEAR_FIRST:
        printf("%d/%d/%d\n", date.year, date.month, date.day);
        break;
    default:
        puts("Wrong format");
    }
}

struct Date readDate()
{
    char input[100] = {0}, ch;
    int pt = 0;
    while ((ch = getchar()) != '\n')
    {
        input[pt++] = ch;
    } // Using scanf string instead
    int year = 0, month = 0, day = 0;
    struct Date res;
    if (sscanf(input, "%d/%d/%d", &month, &day, &year) == 3)
    {
        res.day = day;
        res.month = month;
        res.year = year;
        res.format = MONTH_FIRST;
    }
    else if (sscanf(input, "%d年%d月%d日", &year, &month, &day) == 3)
    {
        res.day = day;
        res.month = month;
        res.year = year;
        res.format = CHINESE;
    }
    else if (sscanf(input, "%d-%d-%d", &year, &month, &day) == 3)
    {
        res.day = day;
        res.month = month;
        res.year = year;
        res.format = YEAR_FIRST;
    } // Can scanf directly into res
    else
    {
        res.format = WRONG;
    }
    return res;
}

int main()
{
    for (int i = 0; i < 1; ++i)
    {
        struct Date date = readDate();
        printDate(date);
    }
}
